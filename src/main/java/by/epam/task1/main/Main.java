package by.epam.task1.main;

import by.epam.task1.entity.appliance.*;
import by.epam.task1.entity.appliance.TypeAppliance;
import by.epam.task1.logic.manager.CreatingApplianceException;
import by.epam.task1.logic.manager.ApplianceManager;
import by.epam.task1.logic.manager.ApplianceNotFoundException;

import java.util.List;

import org.apache.log4j.Logger;



public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);


    public static void main(String[] args) {

        if (logger.isDebugEnabled()) {
            logger.debug("Starting ClientSocket");
        }

        ApplianceManager manager = new ApplianceManager();
        try {
            manager.add(TypeAppliance.COMPUTER, 500, true, 4, 4096);
            manager.add(TypeAppliance.TEAPOT, 200, false, 4);
            manager.add(TypeAppliance.WASHING_MACHINE, 400, false, 4, 20f);
        } catch (CreatingApplianceException e) {
            logger.error("Creating failed", e);
        }

        System.out.println(manager.calculatePower());

        List<Appliance> appliances;
        try {
            appliances = manager.findAppliances(200, true);
            System.out.println(appliances);
        } catch (ApplianceNotFoundException applianceNotFound) {
            logger.info("Appliance not found");
        }

        manager.sortAppliances();

        System.out.println(manager.getContainer().getAppliances());

    }
}