package by.epam.task1.entity.appliance;

/**
 * @author stas-
 * Abstract class for Appliances
 */

public abstract class Appliance {
    /**power of appliance*/
    private final float power;

    /**appliance plugged in or not*/
    private boolean isPluggedIn;

    /**
     * Initialize fields of default appliance
     * @param power current power of appliance
     * @param isPluggedIn shows, if appliance is plugged in
     */
    public Appliance(float power, boolean isPluggedIn) {
        this.power = power;
        this.isPluggedIn = isPluggedIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Appliance appliance = (Appliance) o;

        if (Float.compare(appliance.power, power) != 0)
            return false;

        return isPluggedIn == appliance.isPluggedIn;

    }

    @Override
    public int hashCode() {
        int result = (power != +0.0f ? Float.floatToIntBits(power) : 0);
        result = 31 * result + (isPluggedIn ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "power=" + power +
                ", isPluggedIn=" + isPluggedIn +
                '}';
    }

    public void setIsPluggedIn(boolean isPluggedIn) {
        this.isPluggedIn = isPluggedIn;
    }

    /**
     *
     * @return returns true if appliance is plugged into outlet else return false
     */
    public boolean isPluggedIn() {
        return isPluggedIn;
    }

    /**
     *
     * @return returns current power of appliance
     */
    public float getPower() {
        return power;
    }


}
