package by.epam.task1.entity.appliance;

/**
 * Entity class of hierarchy appliances. Information about teapot
 */
public class Teapot extends Appliance {
    /**
     * teapot's capacity (in liters)
     */
    private final int capacity;

    /**
     * Initialize teapot's characteristics
     * @param capacity teapot's capacity (in liters)
     */
    public Teapot(float power, boolean isPluggedIn, int capacity) {
        super(power, isPluggedIn);
        this.capacity = capacity;
    }

    /**
     *
     * @return returns teapot's capacity (in liters)
     */
    public int getCapacity() {
        return capacity;
    }
}
