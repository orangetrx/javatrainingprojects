package by.epam.task1.entity.appliance;

/**
 * Entity class of hierarchy appliances. Information about computer
 */
public class Computer extends Appliance {
    /**
     * count cores in computer's CPU
     */
    private final int countCores;

    /**
     * size of RAM in computer
     */
    private final int ramSize;

    /**
     * Initialize computer's characteristics
     * @param countCores count cores of computer's CPU
     * @param ramSize size of RAM in computer (in megabytes)
     */
    public Computer(float power, boolean isPluggedIn, int countCores, int ramSize) {
        super(power, isPluggedIn);
        this.countCores = countCores;
        this.ramSize = ramSize;
    }

    /**
     *
     * @return returns count cores of computer's CPU
     */
    public int getCountCores() {
        return countCores;
    }

    /**
     *
     * @return returns size of RAM in computer (in megabytes)
     */
    public int getRamSize() {
        return ramSize;
    }
}
