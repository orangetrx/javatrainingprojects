package by.epam.task1.entity.appliance;

/**
 * Enum for storing appliances types
 */
public enum TypeAppliance {
    COMPUTER,
    TEAPOT,
    WASHING_MACHINE;
}
