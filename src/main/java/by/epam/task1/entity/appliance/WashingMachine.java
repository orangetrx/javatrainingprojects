package by.epam.task1.entity.appliance;

/**
 * Entity class of hierarchy appliances. Information about washing machine
 */
public class WashingMachine extends Appliance {
    /**
     * number of operation modes
     */
    private final int countModes;

    /**
     * machine's volume (in kilograms)
     */
    private final float capacity;

    /**
     * Initialize washing machine's characteristics
     * @param countModes number of operation modes
     * @param capacity machine's volume (in kilograms)
     */
    public WashingMachine(float power, boolean isPluggedIn, int countModes, float capacity) {
        super(power, isPluggedIn);
        this.countModes = countModes;
        this.capacity = capacity;
    }

    /**
     *
     * @return returns number of operation modes
     */
    public int getCountModes() {
        return countModes;
    }

    /**
     *
     * @return returns machine's volume (in kilograms)
     */
    public float getCapacity() {
        return capacity;
    }
}
