package by.epam.task1.entity.container;

import by.epam.task1.entity.appliance.Appliance;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for storing created appliances
 */
public class ApplianceContainer {
    /**
     * Container for appliances
     */
    private  List<Appliance> appliances;

    /**
     * Initialize container
     * @see #appliances
     */
    public ApplianceContainer(){
        appliances = new ArrayList<>();
    }

    /**
     * Adds appliance in container
     * @param appliance current adding appliance
     */
    public void add(Appliance appliance) {
        appliances.add(appliance);
    }

    /**
     *
     * @return returns container
     */
    public  List<Appliance> getAppliances() {
        return appliances;
    }

    /**
     * Replace container
     * @param appliances new container
     */
    public void setAppliances(List<Appliance> appliances) {
        this.appliances = appliances;
    }

    @Override
    public String toString() {
        return  this.getClass().getName() + "{" +
                "appliance=" + appliances +
                '}';
    }
}
