package by.epam.task1.util;

import by.epam.task1.entity.appliance.Appliance;

import java.util.Comparator;

/**
 * Class comparator for sorting container of appliances
 */
public class ApplianceComparator implements Comparator<Appliance> {

    /**
     *
     * @param o1 first appliance
     * @param o2 second appliance
     * @return returns a negative integer, zero, or a positive integer as the power of first appliance argument is less than, equal to, or greater than the power of second appliance
     */
    @Override
    public int compare(Appliance o1, Appliance o2) {
        return (int) (o1.getPower() - o2.getPower());
    }
}
