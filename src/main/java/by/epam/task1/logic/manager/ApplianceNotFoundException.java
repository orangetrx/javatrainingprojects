package by.epam.task1.logic.manager;

/**
 * Exception class for throwing exceptions if container not contains searching appliances
 */
public class ApplianceNotFoundException extends Exception {

    public ApplianceNotFoundException(String message) {
        super(message);
    }

    public ApplianceNotFoundException(String message, Exception ex) {
        super(message, ex);
    }
}
