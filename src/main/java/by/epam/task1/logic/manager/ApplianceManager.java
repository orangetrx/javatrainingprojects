package by.epam.task1.logic.manager;

import by.epam.task1.entity.appliance.TypeAppliance;
import by.epam.task1.logic.factory.AbstractApplianceFactory;
import by.epam.task1.logic.factory.creator.FactoryCreator;
import by.epam.task1.util.ApplianceComparator;
import by.epam.task1.entity.appliance.Appliance;
import by.epam.task1.entity.container.ApplianceContainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for control appliances
 */
public class ApplianceManager {
    /**
     * container for appliances
     * @see ApplianceContainer
     */
    private ApplianceContainer container;

    /**
     * comparator for container
     * @see ApplianceComparator
     */
    private ApplianceComparator comparator;

    /**
     * Initialize container and comparator
     * @see #container
     * @see #comparator
     */
    public ApplianceManager() {
        container = new ApplianceContainer();
        comparator = new ApplianceComparator();
    }

    /**
     * Creates new appliance and adds in container
     * @param typeAppliance type of appliance
     * @param power power of appliance
     * @param isPluggedIn if appliance is plugged in outlet
     * @param capacity teapot's capacity (in liters)
     * @throws CreatingApplianceException if appliance not created
     * @see #add(TypeAppliance, float, boolean, int, int)
     * @see #add(TypeAppliance, float, boolean, int, float)
     */
    public void add(TypeAppliance typeAppliance, float power, boolean isPluggedIn, int capacity) throws CreatingApplianceException {
        AbstractApplianceFactory applianceFactory = FactoryCreator.getFactory(typeAppliance);
        Appliance appliance = applianceFactory.createAppliance(power, isPluggedIn, capacity);
        container.add(appliance);
    }

    /**
     * Creates new appliance and adds in container
     * @param typeAppliance type of appliance
     * @param power power of appliance
     * @param isPluggedIn if appliance is plugged in outlet
     * @param countCores count cores in computer's CPU
     * @param ramSize size of RAM in computer
     * @throws CreatingApplianceException if appliance not created
     * @see #add(TypeAppliance, float, boolean, int)
     * @see #add(TypeAppliance, float, boolean, int, float)
     */
    public void add(TypeAppliance typeAppliance, float power, boolean isPluggedIn, int countCores, int ramSize) throws CreatingApplianceException {
        AbstractApplianceFactory applianceFactory = FactoryCreator.getFactory(typeAppliance);
        Appliance appliance = applianceFactory.createAppliance(power, isPluggedIn, countCores, ramSize);
        container.add(appliance);
    }

    /**
     * Creates new appliance and adds in container
     * @param typeAppliance type of appliance
     * @param power power of appliance
     * @param isPluggedIn if appliance is plugged in outlet
     * @param countModes number of operation modes
     * @param capacity machine's volume (in kilograms)
     * @throws CreatingApplianceException if appliance not created
     * @see #add(TypeAppliance, float, boolean, int)
     * @see #add(TypeAppliance, float, boolean, int, int)
     */
    public void add(TypeAppliance typeAppliance, float power, boolean isPluggedIn, int countModes, float capacity) throws CreatingApplianceException {
        AbstractApplianceFactory applianceFactory = FactoryCreator.getFactory(typeAppliance);
        Appliance appliance = applianceFactory.createAppliance(power, isPluggedIn, countModes, capacity);
        container.add(appliance);
    }

    /**
     * sorts container of appliances
     * @see #container
     * @see #comparator
     */
    public void sortAppliances() {
        Collections.sort(container.getAppliances(), comparator);
    }

    /**
     * @param power power of searching appliance
     * @param isPluggedIn plugged in outlet or not
     * @return returns list of found appliances that appropriate for characteristics
     * @throws ApplianceNotFoundException if aplliance not found
     */
    public List<Appliance> findAppliances(float power, boolean isPluggedIn) throws ApplianceNotFoundException {
        List<Appliance> result = new ArrayList<>();

        for (Appliance appliance : container.getAppliances()) {
            if (appliance.getPower() == power && appliance.isPluggedIn() == isPluggedIn) {
                result.add(appliance);
            }
        }

        if (result.isEmpty())
        {
            throw new ApplianceNotFoundException("Appliances not found");
        }

        return result;
    }

    /**
     *
     * @return returns total power of all appliances in container
     */
    public float calculatePower() {
        float result = 0;

        for (Appliance appliance : container.getAppliances()) {
            result += appliance.getPower();
        }

        return result;
    }

    /**
     *
     * @return returns container of appliances
     */
    public ApplianceContainer getContainer(){
        return container;
    }

}

