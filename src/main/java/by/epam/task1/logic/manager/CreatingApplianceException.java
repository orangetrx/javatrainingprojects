package by.epam.task1.logic.manager;

/**
 * Exception class for throwing exceptions when creating appliance produces error
 */
public class CreatingApplianceException extends Exception {

    public CreatingApplianceException(String message) {
        super(message);
    }

    public CreatingApplianceException(String message, Exception ex) {
        super(message, ex);
    }
}
