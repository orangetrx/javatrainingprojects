package by.epam.task1.logic.factory;

import by.epam.task1.entity.appliance.Appliance;
import by.epam.task1.logic.manager.CreatingApplianceException;

/**
 * Abstract class factory for creating appliances
 */
public abstract class AbstractApplianceFactory {
    /** Method creates computers
     * @param power appliance's power
     * @param isPluggedIn if plugged into outlet
     * @param countCores count cores in computer's CPU
     * @param ramSize size of RAM in computer
     * @return returns appliance
     * @throws CreatingApplianceException if appliance not created
     */
    public  Appliance createAppliance(float power, boolean isPluggedIn, int countCores, int ramSize) throws CreatingApplianceException
    {
        throw new CreatingApplianceException("Creating appliance error");
    }

    /** Method creates washing machines
     * @param power appliance's power
     * @param isPluggedIn if plugged into outlet
     * @param countModes number of operation modes
     * @param capacity machine's volume (in kilograms)
     * @return returns appliance
     * @throws CreatingApplianceException if appliance not created
     */
    public Appliance createAppliance(float power, boolean isPluggedIn, int countModes, float capacity) throws CreatingApplianceException
    {
        throw new CreatingApplianceException("Creating appliance error");
    }

    /** Method creates teapots
     * @param power appliance's power
     * @param isPluggedIn if plugged into outlet
     * @param capacity teapot's capacity (in liters)
     * @return returns appliance
     * @throws CreatingApplianceException if appliance not created
     */
    public Appliance createAppliance(float power, boolean isPluggedIn, int capacity) throws CreatingApplianceException
    {
        throw new CreatingApplianceException("Creating appliance error");
    }
}
