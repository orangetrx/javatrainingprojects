package by.epam.task1.logic.factory;

import by.epam.task1.entity.appliance.Appliance;
import by.epam.task1.entity.appliance.WashingMachine;
import by.epam.task1.logic.manager.CreatingApplianceException;

/**
 * Class factory for creating teapots
 */
public class WashingMachineFactory extends AbstractApplianceFactory {

    /**
     * @return returns created washing machine
     */
    public Appliance createAppliance(float power, boolean isPluggedIn, int countModes, float capacity) throws CreatingApplianceException {
        try {
            return new WashingMachine(power, isPluggedIn, countModes, capacity);
        }
        catch (Exception e)
        {
            throw new CreatingApplianceException(this.getClass().getSimpleName() + " can't create appliance.", e);
        }

    }

}
