package by.epam.task1.logic.factory;

import by.epam.task1.entity.appliance.Appliance;
import by.epam.task1.entity.appliance.Teapot;
import by.epam.task1.logic.manager.CreatingApplianceException;

/**
 * Class factory for creating teapots
 */
public class TeapotFactory extends AbstractApplianceFactory {

    /**
     * @return returns created teapot
     */
    public Appliance createAppliance(float power, boolean isPluggedIn, int capacity) throws CreatingApplianceException{
        try
        {
            return new Teapot(power, isPluggedIn, capacity);
        }
        catch (Exception e)
        {
        throw new CreatingApplianceException(this.getClass().getSimpleName() + " can't create appliance.", e);
        }
    }

}
