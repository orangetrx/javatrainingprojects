package by.epam.task1.logic.factory.creator;

import by.epam.task1.entity.appliance.TypeAppliance;
import by.epam.task1.logic.factory.AbstractApplianceFactory;
import by.epam.task1.logic.factory.ComputerFactory;
import by.epam.task1.logic.factory.TeapotFactory;
import by.epam.task1.logic.factory.WashingMachineFactory;

/**
 * Class for creating factories of appliances
 */
public class FactoryCreator {

    /**
     *
     * @param typeAppliance type of chosen appliance
     * @return returns factory of chosen appliance
     */
    public static AbstractApplianceFactory getFactory(TypeAppliance typeAppliance) {
        AbstractApplianceFactory applianceFactory = null;

        switch (typeAppliance) {
            case COMPUTER:
            {
                applianceFactory = new ComputerFactory();
                break;
            }

            case TEAPOT:
            {
                applianceFactory = new TeapotFactory();
                break;
            }

            case WASHING_MACHINE:
            {
                applianceFactory = new WashingMachineFactory();
                break;
            }

        }

        return applianceFactory;

    }
}
