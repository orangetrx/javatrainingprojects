package by.epam.task1.logic.factory;

import by.epam.task1.entity.appliance.Appliance;
import by.epam.task1.entity.appliance.Computer;
import by.epam.task1.logic.manager.CreatingApplianceException;

/**
 * Class factory for creating computers
 */
public class ComputerFactory extends AbstractApplianceFactory {

    /**
     * @return returns created computer
     */
    @Override
    public Appliance createAppliance(float power, boolean isPluggedIn, int countCores, int ramSize) throws CreatingApplianceException {
        try {
            return new Computer(power, isPluggedIn, countCores, ramSize);
        } catch (Exception e) {
            throw new CreatingApplianceException(this.getClass().getSimpleName() + " can't create appliance.", e);
        }

    }
}
