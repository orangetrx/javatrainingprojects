package by.epam.task2.logic.callcenter;

import by.epam.task2.logic.client.Client;
import org.apache.log4j.Logger;

import java.util.Random;

public class Operator {

    private final static Logger LOGGER = Logger.getLogger(Operator.class.getPackage().getName());

    private int id; //operator's id

    public Operator(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    //cap for operator's operation
    public void helpToClient(Client client) throws InterruptedException {
        LOGGER.debug("Operator #" + id + " is talking with ClientSocket " + client.getClientName());
        Random random = new Random();
        Thread.sleep(1000 + random.nextInt(3000));
    }
}
