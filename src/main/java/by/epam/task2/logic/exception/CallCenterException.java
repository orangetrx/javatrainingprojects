package by.epam.task2.logic.exception;


public class CallCenterException extends Exception {
    public CallCenterException(String message) {
        super(message);
    }

    public CallCenterException(String message, Exception ex) {
        super(message, ex);
    }
}
