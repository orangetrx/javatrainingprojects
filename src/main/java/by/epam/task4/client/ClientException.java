package by.epam.task4.client;

public class ClientException extends Exception {
    public ClientException(String message) {
        super(message);
    }

    public ClientException(String message, Throwable ex) {
        super(message, ex);
    }
}
