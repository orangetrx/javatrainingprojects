package by.epam.task4.client.controller;

import by.epam.task4.client.ClientException;
import entity.message.Operation;
import entity.message.Request;
import entity.message.Response;
import entity.text.Sentence;
import entity.text.Text;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class ClientSocket {
    private static final Logger LOGGER = Logger.getLogger(ClientSocket.class.getPackage().getName());
    private int port;
    private String ip;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private Socket socket;

    private static final String BOOK_PATH = "src\\by\\epam\\task4\\client\\resourses\\c#book";


    public ClientSocket(int port, String ip) {
        this.port = port;
        this.ip = ip;
    }

    public void startClient() throws ClientException {
        try {
            socket = new Socket(ip, port);
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();

            outputStream = new ObjectOutputStream(os);
            inputStream = new ObjectInputStream(is);

            LOGGER.debug("ClientSocket was connected to the server.");

            Operation operation = null;

            do {
                operation = askOperation();
                switch (operation) {
                    case FIND_SENTENCES: {
                        sendRequest(new Request(by.epam.task4.client.util.FileReader.readFile(BOOK_PATH), Operation.FIND_SENTENCES));
                        break;
                    }
                    case SWAP_WORDS: {
                        sendRequest(new Request(by.epam.task4.client.util.FileReader.readFile(BOOK_PATH), Operation.SWAP_WORDS));
                        break;
                    }
                    case EXIT: {
                        sendRequest(new Request(Operation.EXIT));
                        break;
                    }
                }

                if (operation != Operation.EXIT) {
                    getResponse();
                }

            } while (operation != Operation.EXIT);

        } catch (ClientException ex) {
            LOGGER.debug("Sending request failed");
        } catch (IOException e) {
            LOGGER.debug("Reading file failed");
        }
        finally {
            try {
                socket.close();
                LOGGER.debug("Client socket was closed");
            } catch (IOException e) {
                LOGGER.debug("Can't close socket");
            }
        }
    }


    private void sendRequest(Request message) throws ClientException {
        try {
            LOGGER.debug("Sending request to the server..");
            outputStream.writeObject(message);
            outputStream.flush();
            LOGGER.debug("Request has been send to the server. Waiting for response. It may take several time..");
        } catch (IOException e) {
            LOGGER.debug("Sending message failed");
            throw new ClientException("Send message exception", e);
        }
    }

    private void getResponse() {
        try {
            Response response = (Response) inputStream.readObject();
            LOGGER.debug("Response has been get.");
            displayMessage(response);
        } catch (ClassNotFoundException | IOException e) {
            LOGGER.debug("Reading message failed");
        }
    }

    private void displayMessage(Response response) {
        switch (response.getOperation()) {
            case FIND_SENTENCES: {
                System.out.println("Count sentences that have same words");
                List<Sentence> sentences = response.getSentenceList();
                System.out.println(sentences.size());
                break;
            }

            case SWAP_WORDS: {
                Text textResult = response.getText();
                break;
            }
        }
    }

    private Operation askOperation() throws IOException {
        ConsoleHelper.writeMessage("");
        ConsoleHelper.writeMessage("Choose operation:");
        ConsoleHelper.writeMessage(String.format("\t %d - find sentences with same words", Operation.FIND_SENTENCES.ordinal()));
        ConsoleHelper.writeMessage(String.format("\t %d - swap first and last word in sentences", Operation.SWAP_WORDS.ordinal()));
        ConsoleHelper.writeMessage(String.format("\t %d - exit", Operation.EXIT.ordinal()));

        int commandPos = ConsoleHelper.readInt();
        while (commandPos > Operation.values().length - 1) {
            ConsoleHelper.writeMessage("Wrong number of command");
            commandPos = ConsoleHelper.readInt();
        }

        return Operation.values()[commandPos];
    }

}
