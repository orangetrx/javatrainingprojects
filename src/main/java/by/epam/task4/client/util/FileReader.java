package by.epam.task4.client.util;

import by.epam.task4.client.ClientException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReader {

    private static final Logger LOGGER = Logger.getLogger(FileReader.class.getPackage().getName());

    public static String readFile(String filePath) throws ClientException {
        byte[] data = new byte[0];
        try {
            data = Files.readAllBytes(Paths.get(filePath));
        } catch (IOException e) {
            LOGGER.debug("File not found.");
            throw new ClientException("Error while read file", e);
        }
        return new String(data, Charset.forName("UTF-8"));
    }
}
