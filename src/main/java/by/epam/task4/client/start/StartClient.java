package by.epam.task4.client.start;

import by.epam.task4.client.ClientException;
import by.epam.task4.client.controller.ClientSocket;

public class StartClient {
    public static void main(String[] args) throws ClientException {
        ClientSocket clientSocket = new ClientSocket(6666, "127.0.0.1");
        clientSocket.startClient();
    }
}
