package by.epam.task4.server;

/**
 * Created by stas- on 10/16/2015.
 */
public class ServerException extends Exception {
    public ServerException(String message) {
        super(message);
    }

    public ServerException(String message, Throwable ex) {
        super(message, ex);
    }
}
