package by.epam.task4.server.controller.command;
import entity.message.Request;
import entity.message.Response;

public interface Command {
    Response execute(Request request);
}
