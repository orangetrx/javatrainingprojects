package by.epam.task4.server.controller.command;

import by.epam.task4.server.controller.command.impl.FindSentencesCommand;
import by.epam.task4.server.controller.command.impl.SwapWordsCommand;
import entity.message.Operation;

import java.util.HashMap;
import java.util.Map;

public final class CommandHelper {
	private static Map<Operation, Command> commands = new HashMap<Operation, Command>();
	
	static {
		commands.put(Operation.FIND_SENTENCES, new FindSentencesCommand());
		commands.put(Operation.SWAP_WORDS, new SwapWordsCommand());
	}
	
	public static Command getCommand(Operation operation){
		return commands.get(operation);
	}
}
