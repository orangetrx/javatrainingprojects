package by.epam.task4.server.controller.command.impl;

import by.epam.task4.server.controller.command.Command;
import by.epam.task4.server.util.BookParser;
import entity.message.Operation;
import entity.message.Request;
import entity.message.Response;
import entity.text.*;

import java.util.ArrayList;
import java.util.List;

public class FindSentencesCommand implements Command {

    public FindSentencesCommand() {
    }


    @Override
    public Response execute(Request request) {
        BookParser parser = BookParser.getInstance();
        Text text = parser.parse(request.getText());
        List<Sentence> sentences = new ArrayList<>();

        for (PartOfText partOfText : text.getParts()) {
            if (partOfText.getClass() == Paragraph.class) {
                List<PartOfText> sentencesFromParagraph = partOfText.getParts();
                sentences.addAll(getSentencesWithSameWords(sentencesFromParagraph));
            }
        }

        return new Response(sentences, Operation.FIND_SENTENCES);
    }

    private List<Sentence> getSentencesWithSameWords(List<PartOfText> sentencesFromParagraph) {
        List<Sentence> foundSentences = new ArrayList<>();
        for (PartOfText sentence : sentencesFromParagraph) {
            if (hasSameWordsInSentence(sentence)) {
                foundSentences.add((Sentence) sentence);
            }
        }
        return foundSentences;
    }

    private boolean hasSameWordsInSentence(PartOfText sentence) {
        for (PartOfText sentenceItem : sentence.getParts()) {
            if (sentenceItem.getClass() ==  Word.class) {
                if (isWordMeetsMoreThanOneTimeInSentence((Word) sentenceItem, sentence)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isWordMeetsMoreThanOneTimeInSentence(Word word, PartOfText sentence) {
        int countMatches = 0;
        for (PartOfText sentenceItem : sentence.getParts()) {
            if (sentenceItem.equals(word)) {
                countMatches++;
            }
        }
        return countMatches > 1;
    }
}
