package by.epam.task4.server.controller.command.impl;

import by.epam.task4.server.controller.command.Command;
import by.epam.task4.server.util.BookParser;
import entity.message.Operation;
import entity.message.Request;
import entity.message.Response;
import entity.text.Paragraph;
import entity.text.PartOfText;
import entity.text.Text;

import java.util.Collections;
import java.util.List;

public class SwapWordsCommand implements Command {


    public SwapWordsCommand() {
    }

    @Override
    public Response execute(Request request) {
        BookParser parser = BookParser.getInstance();
        Text text = parser.parse(request.getText());

        for (int i = 0 ; i < text.getParts().size(); i++) {
            if (text.getParts().get(i).getClass() == Paragraph.class) {
                Paragraph paragraph = (Paragraph) text.getParts().get(i);
                List<PartOfText> sentencesFromParagraph = paragraph.getParts();
                paragraph.setParts(getSentencesWithSwappedFirstAndLastWords(sentencesFromParagraph));
            }
        }
        return new Response(text, Operation.SWAP_WORDS);
    }

    private List<PartOfText> getSentencesWithSwappedFirstAndLastWords(List<PartOfText> sentences) {
        for (int i = 0; i < sentences.size(); i++) {
            PartOfText sentence = sentences.get(i);
            List<PartOfText> sentenceItems = sentence.getParts();
            if (sentenceItems.size() > 2) {
                Collections.swap(sentenceItems, 0, sentenceItems.size() - 2);
                sentences.set(i, sentence);
            }
        }

        return sentences;
    }
}
