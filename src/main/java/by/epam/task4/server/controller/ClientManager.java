package by.epam.task4.server.controller;

import by.epam.task4.server.ServerException;
import by.epam.task4.server.controller.command.Command;
import by.epam.task4.server.controller.command.CommandHelper;
import entity.message.Operation;
import entity.message.Request;
import entity.message.Response;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientManager extends Thread {
    private static final Logger LOGGER = Logger.getLogger(ClientManager.class.getPackage().getName());
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    public ClientManager(Socket socket) throws ServerException {
        this.socket = socket;
    }

    public void run() {
        try {
            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());

            Operation operation;
            do {
                LOGGER.debug("Getting request from the client..");
                Request request = (Request) inputStream.readObject();
                operation = request.getOperation();

                if (operation != Operation.EXIT) {
                    LOGGER.debug("Request has been get.\nProcessing operation. It may take several time..");
                    Command command = CommandHelper.getCommand(operation);
                    Response response = command.execute(request);
                    sendResult(response);
                }
            } while (operation != Operation.EXIT);

        } catch (ClassNotFoundException | ServerException | IOException e) {
            LOGGER.debug("Client manager error");
        }
        finally {
            try {
                if (socket != null) {
                    socket.close();
                    LOGGER.debug("ClientManager socket was closed");
                }
            } catch (IOException e) {
                LOGGER.debug("Closing socket failed");
            }
        }
    }

    private boolean sendResult(Response response) throws ServerException {

        try {
            LOGGER.debug("Sending response to the client..");
            outputStream.writeObject(response);
            outputStream.flush();
            LOGGER.debug("Response has been send.");
        }
        catch (IOException e) {
            LOGGER.debug("Sending message failed");
            throw new ServerException("Sending message exception", e);
        }

        return true;
    }


}
