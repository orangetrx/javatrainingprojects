package by.epam.task4.server.controller;

import by.epam.task4.server.ServerException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by stas- on 10/15/2015.
 */
public class TextServerSocket {
    private static final Logger LOGGER = Logger.getLogger(TextServerSocket.class.getPackage().getName());
    private int port = 6666;

    public TextServerSocket(int port) {
        this.port = port;
    }

    public void startServer() throws ServerException {

        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                LOGGER.debug("Waiting for connection..");
                Socket socket = serverSocket.accept();
                LOGGER.debug("Client was connected");
                ClientManager clientManager = new ClientManager(socket);
                clientManager.start();
            }
        }
        catch(IOException e){
            LOGGER.debug("Server error");
            throw new ServerException("Server exception", e);
        }
        finally {
            try {
                if (serverSocket != null) {
                    serverSocket.close();
                    LOGGER.debug("Server socket was closed");
                }
            } catch (IOException e) {
                LOGGER.debug("Closing server failed");
            }
        }
    }

}
