package by.epam.task4.server.util;


import entity.text.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BookParser {

    private static final BookParser instance = new BookParser();


    private static final String PATTERN_CODE = "(public|enum|static|using|int\\[|catch|try|namespace|private|protected|class|void|//)+[^,\\{\\}]+\\s*[^;,\\{\\}]+\\{\\s*.*?\\}";
    private static final String PATTERN_SENTENCE = "\\b";
    private static final String PATTERN_WORD = "\\w+";
    private static final String PATTERN_PARAGRAPH = "[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)";

    private BookParser() {}

    public static BookParser getInstance() {
        return instance;
    }


    public Text parse(String textBook){

        Text text = new Text();

        int startPositionOfText = 0;
        Pattern codePattern = Pattern.compile(PATTERN_CODE, Pattern.DOTALL);
        Matcher matcher = codePattern.matcher(textBook);
        
        while (matcher.find()) {
            String codeBlock = matcher.group();

            String textBeforeCode = textBook.substring(startPositionOfText, textBook.indexOf(codeBlock, startPositionOfText));
            startPositionOfText = textBook.indexOf(codeBlock, startPositionOfText) + codeBlock.length();

            String[] paragraphs = textBeforeCode.split("\\n");

            for (String paragraphString: paragraphs) {
                text.add(parseParagraph(paragraphString));
            }

            text.add(new CodeExample(codeBlock));
        }

        String otherText = textBook.substring(startPositionOfText, textBook.length());
        String[] paragraphs = otherText.split("\\n");

        for (String paragraphString: paragraphs) {
            text.add(parseParagraph(paragraphString));
        }

        return text;
    }

    private Paragraph parseParagraph(String paragraphString) {
        Paragraph paragraph = new Paragraph();
        Pattern paragraphPattern = Pattern.compile(PATTERN_PARAGRAPH);
        Matcher matcher = paragraphPattern.matcher(paragraphString);
        while (matcher.find()) {
            String sentenceString = matcher.group();
            paragraph.add(parseSentence(sentenceString));
        }
        return paragraph;
    }

    private Sentence parseSentence(String sentenceString) {
        Sentence sentence = new Sentence();

        for (String item: sentenceString.split(PATTERN_SENTENCE)) {
            if (item.matches(PATTERN_WORD)) {
                sentence.add(new Word(item));
            }
            else {
                sentence.add(new Punctuation(item));
            }
        }

        return sentence;
    }

}

