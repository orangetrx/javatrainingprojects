package by.epam.task4.server.start;

import by.epam.task4.server.ServerException;
import by.epam.task4.server.controller.TextServerSocket;

/**
 * Created by stas- on 10/20/2015.
 */
public class StartServer {
    public static void main(String[] args) throws ServerException {
        TextServerSocket server = new TextServerSocket(6666);
        server.startServer();
    }
}
